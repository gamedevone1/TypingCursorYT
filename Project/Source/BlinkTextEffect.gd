tool
extends RichTextEffect
class_name RichTextGhost

# Syntax: [blink freq=10.0][/blink]

# Define the tag name.
var bbcode = "blink"

func _process_custom_fx(char_fx):
	# Store how fast the character should blink from the tag
	var freq = char_fx.env.get("freq", 10.0)

	# Calculate alpha over time
	var alpha = sin(char_fx.elapsed_time * freq) * 0.5 + 0.5
	char_fx.color.a = alpha
	
	# Character sucessfully transformed
	return true
