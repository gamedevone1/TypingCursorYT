extends RichTextLabel

const ColorTagStart = "[color=white]"
const ColorTagEnd = "[/color]"

const Cursor = "[blink freq=5.0]" + "|" + "[/blink]"
var OutputString: String = ""

func _input(event):
	
	# If keyboard is pressed, store typed keyboard input
	var KeyTyped: String = ""
	if event is InputEventKey and event.is_pressed():
		
		# ***OPTIONAL***
		# If Backspace was pressed, removed last typed keyboard input
		if event.scancode == KEY_BACKSPACE:
			OutputString.erase(OutputString.length() - 1, 1)
		
		# ***OPTIONAL***
		# If Enter was pressed, move to the next line
		elif event.scancode == KEY_ENTER:
			KeyTyped = "\n"
		
		# Otherwise, store typed keyboard input
		else:
			KeyTyped = PoolByteArray([event.unicode]).get_string_from_utf8()
		
		# Add last key typed to output string
		OutputString += KeyTyped
		
		# Add Text Color
		# Add Blinking Cursor
		# Parse string with BBCode Tags
		var err = parse_bbcode(ColorTagStart + OutputString + ColorTagEnd + Cursor)
		assert(err == OK, "Failed to parse bbcode in string. Error Code: %d" % err)
