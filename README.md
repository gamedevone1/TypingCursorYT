# Godot Blinking Cursor Example

Example project of custom Blinking Cursor in Godot.

![](Media/TypingCursorExample.gif)

## Prerequisites

- Godot 3.4.x or newer

## Installation

1. [Download](https://gitlab.com/gamedevone1/TypingCursorYT/-/archive/main/TypingCursorYT-main.zip) and Extract ZIP file into a folder.
2. Run Godot 3.4.x or newer
3. Select Import Project
   1. ```project.godot``` file is located in the ```Project/``` directory

## License

The MIT License (MIT)

Copyright (c) 2022 Devone Reynolds


